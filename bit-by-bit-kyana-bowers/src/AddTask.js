import * as React from "react";
import PropTypes from "prop-types";

const AddTasks = ({ updateTask }) => {
  const [taskName, setTaskName] = React.useState();
  const handleOnChangedEvent = ({ target: { value } }) => setTaskName(value);
  const handleOnClickAdd = () => updateTask(taskName);

  return (
    <div>
      <h3>Add Task</h3>
      <input name="taskName" value={taskName} onChange={handleOnChangedEvent} />
      <button onClick={handleOnClickAdd}>Add Task</button>
    </div>
  );
};

AddTasks.propTypes = {
  updateTask: PropTypes.func,
};

AddTasks.defaultProps = {
  updateTask: () => {},
};

export default AddTasks;
