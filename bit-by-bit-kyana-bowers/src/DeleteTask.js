import * as React from "react";
import PropTypes from "prop-types";

const DeleteTasks = ({ deleteTask }) => {
  const taskID = React.useState();
  const handleOnClickDelete = () => deleteTask(taskID);

  return (
    <div>
      <button onClick={handleOnClickDelete}>Delete Task</button>
    </div>
  );
};

DeleteTasks.propTypes = {
  deleteTask: PropTypes.func,
};

DeleteTasks.defaultProps = {
  deleteTask: () => {},
};

export default DeleteTasks;
