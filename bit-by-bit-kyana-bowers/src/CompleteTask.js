import * as React from "react";
import PropTypes from "prop-types";

const CompleteTasks = ({ completeTask }) => {
  const taskID = React.useState();

  return (
    <label for="taskID">
      <input type="checkbox" id={taskID} name="isCompletedTask" value="yes" />
      Complete Task
    </label>
  );
};

CompleteTasks.propTypes = {
  completeTask: PropTypes.func,
};

CompleteTasks.defaultProps = {
  completeTask: () => {},
};

export default CompleteTasks;
