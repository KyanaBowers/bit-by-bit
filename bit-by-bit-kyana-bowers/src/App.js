import "./App.css";
import AddTasks from "./AddTask.js";
import DeleteTasks from "./DeleteTask.js";
import React from "react";

const ViewTasks = (props) => {
  const { tasks = [] } = props;

  return (
    <ul>
      {tasks.map((task) => {
        function CallDeleteProps() {
          props.removeItemFromTaskList(task.id);
        }

        return (
          <div>
            <li key={task.id}> {task.name}</li>
            <DeleteTasks deleteTask={CallDeleteProps} />
          </div>
        );
      })}
    </ul>
  );
};

const App = () => {
  const [taskList, setTaskList] = React.useState([]);
  const addNewItemToTodoList = (taskName) => {
    const updatedList = [...taskList, { id: taskList.length, name: taskName }];
    setTaskList(updatedList);
  };

  const deleteTask = (taskID) => {
    setTaskList((taskList) => {
      const newTaskList = taskList.filter((task) => task.id !== taskID);
      return newTaskList;
    });
  };

  const completeTask = (taskID) => {};

  return (
    <div>
      <AddTasks updateTask={addNewItemToTodoList} />
      <ViewTasks tasks={taskList} removeItemFromTaskList={deleteTask} />
    </div>
  );
};

export default App;
